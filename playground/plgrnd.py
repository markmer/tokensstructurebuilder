# from scipy.signal import argrelextrema
import numpy as np
import matplotlib.pyplot as plt
# from sklearn.linear_model import LinearRegression
# # import __init__
# 
# 
# from additional.default.geneticOperators._supplementary.FrequencyProcessor import FrequencyProcessor4TimeSeries as fp
# from scipy.signal import square, sawtooth
# 
# # 
# #
# grid = np.linspace(0.1, 10, 100)
#    # x = -(2*0.1*grid + np.cos(grid))
# # x -= x.mean()
# # x /= np.abs(x).max()
# x = (np.sin(grid) + 1*np.cos(grid))
# y = np.cos(grid) - 1*np.sin(grid)
# 
# 
# 
# 
# x, y = np.meshgrid(x, y)
# x = x.reshape((-1,))
# y = y.reshape((-1,))
# 
# z = (x - y)**2
# 
# # x = square(grid)
# # y = square(grid + np.pi/3)
# # z = square(grid + np.pi/3)
# 
# 
# # x = sawtooth(grid)
# # y = sawtooth(grid + np.pi/2)
# # z = sawtooth(grid + np.pi/3)
# 
# 
# # fp.fft(grid, x, wmax=1, wmin=-1)
# # fp.fft(grid, y, wmax=1, wmin=-1)
# 
# 
# plt.figure('TS')
# # plt.plot(x)
# # plt.plot(y)
# plt.scatter(x, y, c=z)
# plt.show()
# 
# # freq = fp.choice_freq_for_summand(grid, x, max_len=2, choice_size=2)
# # print(freq)
# 
# 
# #
# #
# # x = np.sin(2*grid)
# # y = np.sin(grid + 0*np.pi/2)
# # z = np.sin(grid + np.pi/3)
# #
# # features = np.array([x, y])
# #
# # model = LinearRegression()
# # model.fit(features.T, z)
# #
# #
# # # plt.plot(x)
# # # plt.plot(y)
# # plt.plot(model.coef_[0]*x+model.coef_[1]*y)
# # plt.plot(z)
# # plt.show()
# # print(model.coef_)
#



# grid = np.linspace(0, 100, 1000)
#
# fig, axs = plt.subplots(2, 2)
#
# w = 1
#
# x = np.sin(w*grid)
# noise = np.random.normal(0, 0.5, len(x))
# # noise = np.sin(0.1*w*grid)
#
# xAn = x + noise
# xFn = np.sin(w*grid + noise)
#
# ts = xFn
# for idx, ts in enumerate([xAn, xFn]):
#     axs[idx, 0].plot(grid, ts, label='noised')
#     axs[idx, 0].plot(grid, x, label='original')
#     axs[idx, 0].set_xlabel('t, s')
#     axs[idx, 0].set_ylabel('Series')
#     axs[idx, 0].grid(True)
#     axs[idx, 0].legend()
#     axs[idx, 0].set_title('original and noised data')
#
#     axs[idx, 1].hist(ts-x, label='residuals')
#     axs[idx, 1].hist(noise, label='original noise')
#     axs[idx, 1].set_xlabel('value')
#     axs[idx, 1].set_ylabel('density')
#     axs[idx, 1].grid(True)
#     axs[idx, 1].legend()
#     axs[idx, 1].set_title('Residuals distribution')
#
#
# plt.tight_layout()
# plt.show()



from buildingBlocks.default.Tokens import Constant, Sin, Product, Imp, set_constants
grid = np.linspace(0.1, 100, 1000)


impv = Imp(params=np.array([1., 0.1, 0.5, 0.5, 0, 0, 0])).value(grid)

plt.plot(grid, impv)
plt.show()

