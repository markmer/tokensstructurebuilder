# from copy import deepcopy
# from multiprocessing import Pool, Process, current_process, active_children, get_context
# import sys
# import functools
# import numpy as np
# import time
#
#
# workers = 2
#
# if current_process().name == 'MainProcess':
#     if workers == 0:
#         pool = None
#     else:
#         pool = Pool(workers)
#         # pool.terminate()
#         # pool.close()
#         print('NEW POOL', flush=True)
#         print(pool, flush=True)
#         print(current_process(), active_children(), flush=True)
#         print('Count Processes-->', len(active_children()))
#
# class NewMapWrapper:
#
#     def __init__(self):
#         pass
#
#     # @staticmethod
#     # def _copy_func(f, name, qualname):
#     #     import types
#     #     """Based on http://stackoverflow.com/a/6528148/190597 (Glenn Maynard)"""
#     #     g = types.FunctionType(f.__code__, f.__globals__, name=name,
#     #                            argdefs=f.__defaults__,
#     #                            closure=f.__closure__)
#     #     g.__qualname__ = qualname
#     #     g.__kwdefaults__ = f.__kwdefaults__
#     #     return g
#
#     def __call__(self, func):
#         if current_process().name != 'MainProcess':
#             @functools.wraps(func)
#             def n_func(self, *args, **kwargs): return []
#             return n_func
#
#         # @FunctionWrapper()
#         @functools.wraps(func)
#         def wrapper(self, *args, **kwargs):
#             global workers
#             global pool
#             if workers == 0:
#                 # return list(map(func_copy, kwargs['population']))
#                 return func(self, kwargs['population'])
#             ret = list(pool.map(functools.partial(func, self),
#                                 np.array_split(kwargs['population'], len(active_children()))))
#             try:
#                 ret = [item for sublist in ret for item in sublist]
#             except:
#                 pass
#             # pool.terminate()
#             # pool.close()
#             # pool.join()
#             # delattr(sys.modules['__main__'], func_copy.__qualname__)
#             return ret
#
#         return wrapper
#
#
#
# def decor(func):
#     def wrapper(self, *args, **kwargs):
#         print('decor')
#         func(self, *args, **kwargs)
#     return wrapper
#
#
#
# class A:
#
#     def f(self, population=[10]):
#         print(current_process().name + '------>' + __name__, population)
#         time.sleep(0.1)
#
#     def b(self, l):
#         l.append(10)
#
#
#     # new_f = NewMapWrapper()(f)
#
# a = A()
# # a.f(population=[1, 2, 3])
# # a.new_f(population=[i for i in range(10)])
# l = [1]
# a.b(l)
# print(l)
#
# if __name__ == '__main__':
#     print(A.f)
#     # print(A.new_f)

import numpy as np
import matplotlib.pyplot as plt
from buildingBlocks.default.Individs import Equation
from buildingBlocks.default.Tokens import Constant, Sin, Product
# from additional.baseline.GeneticOperators import OperatorsMapping as OM_baseline
from buildingBlocks.default.geneticOperators import FitnessEvaluators
from buildingBlocks.default.geneticOperators.OperatorsMappings import OperatorsMapping\
    as OM_default, set_operators_mapper
from buildingBlocks.default.geneticOperators.Optimizers import PeriodicTokensOptimizerIndivid

from sklearn.linear_model import Lasso, LinearRegression

# определяем вычислительную сетку
grid = np.linspace(0, 100, 1000)
# grid = grid.reshape(2, 500)

# определяем токены
# token1 = Constant(val=np.sin(grid), name_='sin', mandatory=1.21215)
# token3 = Constant(val=np.cos(grid), name_='cos')
# token2 = Sin(optimize_id=1)

m1 = np.random.normal(0, 1, grid.shape)
m2 = np.random.normal(0, 1, grid.shape)
r = np.random.normal(0, 1, grid.shape)

F = m1*m2/r

token1 = Constant(val=m1, name_='m1', mandatory=1.11)
token2 = Constant(val=m2, name_='m2', mandatory=3.5)
token3 = Constant(val=r, name_='r', mandatory=2.2)
token4 = Constant(val=F, name_='F', mandatory=5.11)



operatorsMap = OM_default()
operatorsMap.periodicTokensOptimizerIndivid = PeriodicTokensOptimizerIndivid(params=dict(grid=grid,
                                                                                         optimize_id=1))
operatorsMap.fitnessIndivid = FitnessEvaluators.MSEFitnessIndivid(params=dict(grid=grid))
set_operators_mapper(operatorsMap)

ind_genetic_operators = {
    'optimize': 'periodicTokensOptimizerIndivid',
    'fitness': 'fitnessIndivid'
}

ind = Equation(genetic_operators=ind_genetic_operators, chromo=[token1, token2, token3])

print(ind.formula(True))

ind.apply_operator(name='optimize')
print(ind.formula(True))
# ind.apply_operator(name='optimize')
# print(ind.formula(True))

for token in ind.chromo:
    plt.plot(token.value(grid))

plt.plot(ind.value(grid))
plt.show()

