from copy import deepcopy

from sklearn.preprocessing import normalize

from buildingBlocks.baseline.Tokens import ComplexToken
from buildingBlocks.default.geneticOperators.OperatorsMappings import OperatorsMapping\
    as OM_default, set_operators_mapper, OperatorsMapKeeper
from buildingBlocks.default.Tokens import Constant, Sin, Product, Imp, set_constants
from buildingBlocks.default.Individs import Equation
from buildingBlocks.default.Populations import PopulationOfEquations
from buildingBlocks.default.geneticOperators import (Crossovers, FitnessEvaluators,
                                                     Initializers, Mutations, Regularizations,
                                                     Selectors)
from buildingBlocks.baseline.GeneticOperators import OperatorsMapping as OM_baseline
from buildingBlocks.baseline.Wrappers import workersKeeper, set_workers
from buildingBlocks.default.geneticOperators.Optimizers import PeriodicTokensOptimizerIndivid, \
    PeriodicTokensOptimizerPopulation, PeriodicInProductTokensOptimizerIndivid

import numpy as np
import matplotlib.pyplot as plt
import sys
from time import perf_counter

# Определим число процессов
set_workers(0)

# определяем вычислительную сетку
grid = np.linspace(0.1, 100, 1000)
# grid = grid.reshape(2, 500)

# определяем константы в специальном классе для использования их в токенах (размерность векторов должна совпадать
# с сеткой)
impv = Imp(params=np.array([1., 0.1, 0.5, 0.5, 0.5, 1, 0])).value(grid)
set_constants(sine=np.sin(grid), cosine=np.cos(grid), target=1*np.sin(grid) + 1*np.sin(2*grid) + impv)

# ------> Начало блоков с определениями токенов


#------> начало примера
# тут можно поиграться с разными тригонометрическими уравнениями

# token4 = Constant(val=np.sin(grid) + 2*np.cos(grid), name_='(sin+Ccos)', mandatory=1.21215)
# token3 = Constant(val=np.cos(grid) - 2*np.sin(grid), name_='(cos-Csin)', mandatory=2.22)
# token1 = Constant(val=np.sin(grid), name_='sin', mandatory=2.3432)
# token2 = Constant(val=np.cos(grid), name_='cos', mandatory=1.445)

# testing constants
token1 = Constant(val=None, name_='target', mandatory=2.3432)
token2 = Constant(val=None, name_='cosine', mandatory=1.445)

# token3 = Constant(val=np.ones(grid.shape)+0*np.random.normal(0, 0.01, grid.shape), name_='ones', mandatory=3.33)
#
token5 = Sin(optimize_id=1, name_='Sin', params=np.array([0., 0., 0.]))
token6 = Imp(optimize_id=1, name_='Imp')
# token3 = Constant(val=0.1*grid, name_='trend', mandatory=3.33)
# token4 = Constant(val=np.cos(grid)+np.sin(grid)+0.1*grid, name_='(trend+cos+sin)', mandatory=4.44)

# <------- конец примера


# #------> начало примера
#
# # Некореллированный набор с помощью шумов
# m1 = np.random.normal(0, 1, grid.shape)
# m2 = np.random.normal(0, 1, grid.shape)
# r = 3.5 + np.random.normal(0, 1, grid.shape)
#
# # Некореллированный набор с помощью ортогональных гармонических функций
# # m1 = np.sin(grid)
# # m2 = np.sin(2.3*grid)
# # r = 2+np.sin(3.2*grid)
#
# F = (m1*m2 + m2 + 1*np.sin(grid))/(r*r) + m1
#
#
# token1 = Constant(val=m1, name_='m1', mandatory=1.11)
# token2 = Constant(val=m2, name_='m2', mandatory=3.5)
# token3 = Constant(val=r, name_='r', mandatory=2.2)
# token4 = Constant(val=F, name_='F', mandatory=5.11)
# # <------- конец примера


# <------- Конец блоков с определениями токенов




# Задаем полный набор токенов из которых будем собирать выражение
tokens = [token1, token5, token6]


# Задаем максимальную глубину сомножителей, тип токенов которые могут быть в произведении и сам токен отвечающий
# за произведение
maxlen_subtokens = 1
p_tokens = [token1]
product_token = Product(params=np.array([1., maxlen_subtokens]))


# Посмотреть визуально на стартовые токены
# plt.figure('start')
# for token in tokens:
#     plt.plot(token.value(grid), label=token.name())
# plt.legend()
# plt.show()


# Словарь соотношений имен операторов в индивиде и имен операторов в хранителе операторов
ind_genetic_operators = {
    'fitness': 'fitnessIndivid',
    'initialize': 'initializeIndivid',
    'mutation': 'mutationIndivid',
    'crossover': 'crossoverIndivid',
    'LASSO': 'lassoIndivid',
    'DelDuplicate': 'delDuplicateTokensIndivid',
    'product mutation': 'productMutationIndivid',
    'CheckMandatory': 'checkMandatoryTokensIndivid',
    'optimize': 'periodicTokensOptimizerIndivid',
    'optimizeProduct': 'periodicInProductTokensOptimizerIndivid'
}

# создаем индивида
individ = Equation(genetic_operators=ind_genetic_operators)

population_size = 10

# определяем ген операторы для популяции в хранителе операторов
# определяется тут сразу для всех процессов (особенно для винды)
operatorsMap = OM_default()
operatorsMap.fitnessIndivid = FitnessEvaluators.MSEFitnessIndivid(params=dict(grid=grid))
operatorsMap.fitnessPopulation = FitnessEvaluators.FitnessPopulation()
operatorsMap.initializeIndivid = Initializers.InitIndivid()
operatorsMap.initializePopulation = Initializers.InitPopulation(params=dict(population_size=population_size,
                                                                            individ=individ))
operatorsMap.mutationIndivid = Mutations.MutationIndivid(params=dict(mut_intensive=2,
                                                                     increase_prob=0.7,
                                                                     tokens=tokens))
operatorsMap.mutationPopulation = Mutations.MutationPopulation(params=dict(mutation_size=int(0.3*population_size)+1))
operatorsMap.crossoverIndivid = Crossovers.CrossoverIndivid(params=dict(cross_intensive=2,
                                                                        increase_prob=0.7))
operatorsMap.crossoverPopulation = Crossovers.CrossoverPopulation(params=dict(crossover_size=int(0.4*population_size)+1))
operatorsMap.selectionPopulation = Selectors.RouletteWheelSelection(params=dict(tournament_size=population_size,
                                                                                winners_size=int(0.5*population_size)+1))
operatorsMap.delDuplicateTokensIndivid = Regularizations.DelDuplicateTokensIndivid()
operatorsMap.checkMandatoryTokensIndivid = Regularizations.CheckMandatoryTokensIndivid(params=dict(tokens=tokens,
                                                                                                   add_to_complex_prob=0.5))
operatorsMap.regularisationPopulation = Regularizations.RegularisationPopulation(params=dict(parallelise=False))
operatorsMap.elitismPopulation = Selectors.Elitism(params=dict(elitism=1))
operatorsMap.restrictionPopulation = Selectors.RestrictPopulation(params=dict(population_size=population_size))

operatorsMap.lassoIndivid = Regularizations.LassoIndivid(params=dict(grid=grid,
                                                                     regularisation_coef=0.0001))
# operatorsMap.lassoIndivid = Regularizations.DEOptIndivid(params=dict(grid=grid))
operatorsMap.lassoPopulation = Regularizations.LassoPopulation(params=dict(parallelise=True))
operatorsMap.productMutationIndivid = Mutations.ProductTokenMutationIndivid(params=dict(tokens=p_tokens,
                                                                                        product_token=product_token,
                                                                                        mut_prob=0.3,
                                                                                        max_multi_len=maxlen_subtokens))
operatorsMap.periodicTokensOptimizerIndivid = PeriodicTokensOptimizerIndivid(params=dict(grid=grid,
                                                                                         optimize_id=1,
                                                                                         optimizer='DE',
                                                                                         popsize=2))
operatorsMap.periodicInProductTokensOptimizerIndivid = PeriodicInProductTokensOptimizerIndivid(params=dict(grid=grid,
                                                                                                           optimize_id=1,
                                                                                                           optimizer='DE',
                                                                                                           complex_tokens_types=[type(product_token)]))
operatorsMap.periodicTokensOptimizerPopulation = PeriodicTokensOptimizerPopulation(params=dict(parallelise=True))

set_operators_mapper(operatorsMap)


# Словарь соотношений имен операторов в популяции и имен операторов в хранителе операторов
# Сделан для быстрого межпроцессного общения (передача словаря вместо передача объектов-оптимизаторов)
pop_genetic_operators = {
     'fitness': 'fitnessPopulation',
     'initialize': 'initializePopulation',
     'elitism': 'elitismPopulation',
     'selection': 'selectionPopulation',
     'mutation': 'mutationPopulation',
     'crossover': 'crossoverPopulation',
     'restriction': 'restrictionPopulation',
     'regularization': 'regularisationPopulation',
     'optimize': 'periodicTokensOptimizerPopulation',
     'lasso': 'lassoPopulation'
}

# создаем популяцию
population = PopulationOfEquations(genetic_operators=pop_genetic_operators, iterations=10)

if __name__ == '__main__':
    time = perf_counter()
    population.evolutionary()
    time = perf_counter() - time

    best = list(filter(lambda individ: individ.elitism, population.population))
    print('\nbest\n')
    for best in best:
        # print(best.kind)
        try:
            for form in best.forms:
                print(form)
        except:
            pass
        print('----\n')
        print(best.formula(), best.fitness)
        best.chromo.sort(key=lambda token: -abs(token.param(name='Amplitude')))
        for token in best.chromo:
            print(token.name())
        # print('mandatories')
        # for token in best.chromo:
        #     print(token.mandatory)

    print('TIME--> {}'.format(time))

    # best = best[0]
    plt.figure('end')
    plt.plot(best.value(grid))
    # plt.plot(token1.value(grid)*token1.value(grid))
    plt.show()

    # plt.figure('endend')
    # plt.plot(best.value(grid))
    # # plt.plot(token1.value(grid)*token1.value(grid))
    # plt.show()


# тестовый брутфорс
# TODO: make new class to incapsulate this method
# if __name__ == '__main__':
#
#     def get_next_set_of_tokens(init_tokens, step_tokens, slices):
#         res = []
#         print(slices)
#         c_init_tokens = deepcopy(init_tokens)
#         c_init_tokens.reverse()
#         ret_slices = []
#         for idx, add_token in enumerate(step_tokens):
#             res.append(deepcopy(c_init_tokens[:slices[idx]]))
#             for tidx, token in enumerate(res[-1]):
#                 if isinstance(token, ComplexToken):
#                     token.add_subtoken(add_token.copy())
#                 else:
#                     res[-1][tidx] = Product(subtokens=[token.copy(), add_token.copy()])
#             res[-1].reverse()
#         slices.reverse()
#         ret_slices = [0 for _ in range(len(slices))]
#         for i in range(len(ret_slices)):
#             ret_slices[i] = np.sum(slices[:i+1])
#         ret_slices.reverse()
#         res = [item for sublist in res for item in sublist]
#         return res, ret_slices
#
#     def get_full_set_of_tokens(step_tokens, max_n):
#         init_tokens = deepcopy(step_tokens)
#         N = len(step_tokens)
#         slices = [N - i for i in range(N)]
#         res = [deepcopy(init_tokens)]
#         for i in range(1, max_n):
#             init_tokens, slices = get_next_set_of_tokens(init_tokens, step_tokens, slices)
#             res.append(init_tokens)
#         res = [item for sublist in res for item in sublist]
#         return res
#
#     # step_tokens = deepcopy(tokens)
#     # res = get_full_set_of_tokens(step_tokens, 2)
#     # for idx, token in enumerate(res):
#     #     print(idx, token.name())
#     # print(len(res))
#
#     # res = deepcopy(tokens)
#
#     res = [Product(subtokens=[token5, token4]), token3]
#
#     ind = Equation(chromo=res,
#                    genetic_operators=ind_genetic_operators)
#     # ind = Equation(chromo=[token1, token2, token3, token4],
#     #                genetic_operators=ind_genetic_operators)
#     print(ind.formula())
#     time = perf_counter()
#     ind.apply_operator(name='optimizeProduct')
#     time = perf_counter() - time
#     print(ind.formula())
#     print(ind.forms)
#     ind.chromo.sort(key=lambda token: -abs(token.param(name='Amplitude')))
#     for token in ind.chromo:
#         print(token.name())
#     print(ind.fitness)
#     print('TIME-->', time)
#
#     # print(ind.chromo.count(token6))
#
#     plt.figure('end')
#     plt.plot(ind.value(grid))
#     # plt.plot(token1.value(grid)*token1.value(grid))
#
#
#     plt.figure('tokens')
#     for token in ind.chromo:
#         plt.plot(token.value(grid), label=token.name())
#     plt.legend()
#     plt.show()
#     #
#     # features = np.array(list(map(lambda token: token.value(grid), ind.chromo)))
#     # features -= np.mean(features, axis=1, keepdims=True)
#     # features, norms = normalize(features, norm='l2', axis=1, return_norm=True)
#     #
#     # plt.figure('Lasso features')
#     # plt.plot(features.T)
#     # plt.show()


