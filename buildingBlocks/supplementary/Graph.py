from copy import deepcopy
from functools import reduce

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

import buildingBlocks.default.Tokens as tokensPool
from buildingBlocks.default.Individs import Equation
from buildingBlocks.default.geneticOperators.Optimizers import PeriodicInProductTokensOptimizerIndivid as piptoi
from buildingBlocks.default.geneticOperators._supplementary.FrequencyProcessor import FrequencyProcessor4TimeSeries as fp

Product = tokensPool.Product
fitness_evaluator = piptoi._fitness_wrapper

grid = np.linspace(0.1, 100, 1000)

noiss = [np.random.normal(0, 1, grid.shape) for i in range(2)]
# noiss = [np.random.normal(0, 1) for i in range(2)]
# print(noiss)

def noise():
    count = 0
    while True:
        count += 1
        yield noiss[count % 2]

gen = noise()

# print(next(gen))
# print(next(gen))

# for i in gen:
#     print(i)

def param_grid(bounds, n):
    x = np.linspace(bounds[0][0], bounds[0][1], n)
    y = np.linspace(bounds[1][0], bounds[1][1], n)
    return np.meshgrid(x, y)

def fitness_grid(individ ,info):
    token_x, idx_x, x = info[0]
    token_y, idx_y, y = info[1]
    z = np.zeros(x.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            X = x[i, j]
            Y = y[i, j]
            token_x.set_param(X, idx=idx_x)
            token_y.set_param(Y, idx=idx_y)
            z[i, j] = np.var(reduce(lambda v1, v2: v1+v2,
                                    list(map(lambda token: token.value(grid) + next(gen),
                                             individ.chromo))))
    return z

tokensPool.set_constants(sin=np.sin(grid), cos=np.cos(grid))

# построение токена
token1 = tokensPool.Constant(name_='sin')
token2 = tokensPool.Constant(name_='cos')

# token1 = tokensPool.Constant(val=np.sin(grid)+1*np.cos(grid), name_='(sin+Ccos)')
# token2 = tokensPool.Constant(val=np.cos(grid)-1*np.sin(grid), name_='(cos-Csin)')

token3 = tokensPool.Sin(params=np.array([1., 1/(2*np.pi), 0.5]))
token4 = tokensPool.Sin(params=np.array([1., 1/(2*np.pi), 0]))

token5 = Product(subtokens=[token1, token3])
token6 = Product(subtokens=[token2, token4])

token5.cache_val = False
token6.cache_val = False


ind = Equation(chromo=[token5, token6])

x, y = param_grid(((0, 2), (0, 2*np.pi)), n=100)


# z = fitness_grid(ind, ((token4, 0, x), (token4, 2, y)))

# -----------

def fitness_grid2(x, y, target):
    z = np.zeros(x.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            X = x[i, j]
            Y = y[i, j]
            z[i, j] = np.var(Y*np.sin(X*grid)+target)
    return z

target = np.sin(grid + 0*np.pi) + 0*np.random.normal(0, 2, grid.shape)
z = fitness_grid2(x, y, target)

plt.figure('targ')
plt.plot(grid, target)

# # print(x)
# # print(y)
# # print(z)
#
# -------->
# x = x.reshape((-1,))
# y = y.reshape((-1,))
# z = z.reshape((-1,))
#
# plt.scatter(x, y, c=z)
# plt.show()

# <--------------


# token3.set_param(1, idx=0)
# # token3.set_param(1, idx=2)
#
# token4.set_param(1, idx=0)
# # token4.set_param(0, idx=2)
# plt.plot(ind.value(grid))
# for token in [token1, token2, token3, token4]:
#     plt.plot(token.value(grid), label=token.name(True))
# plt.legend()
# print(ind.formula())
#
#
fig = plt.figure('3d')
ax = fig.gca(projection='3d')
# Plot the surface.
surf = ax.plot_surface(x, y, z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)


# Customize the z axis.
# ax.set_zlim(-1.01, 1.01)
# ax.zaxis.set_major_locator(LinearLocator(10))
# ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)




# fig, axs = plt.subplots(3, 2)
#
# for idx, p2 in enumerate([0.5, 0.7, 0.95]):
#     token = tokensPool.Imp(params=np.array([1, 1 / (2 * np.pi), p2, 0.5, 0, 0, 0]))
#     token_value = token.value(grid)
#     token_value -= np.mean(token_value)
#
#     axs[idx, 0].plot(grid, token_value)
#     axs[idx, 0].set_xlabel('t, s')
#     axs[idx, 0].set_ylabel('Series')
#     axs[idx, 0].grid(True)
#
#     freqs, spec = fp.fft(grid, token_value, 0, 2)
#     spec = np.abs(spec)
#     axs[idx, 1].plot(2*np.pi*freqs, spec)
#     axs[idx, 1].set_xlabel('freq, Hz')
#     axs[idx, 1].set_ylabel('Spectra')
#     axs[idx, 1].grid(True)


#------>


# fig, axs = plt.subplots(1, 2)
#
# trend = tokensPool.Power(params=np.array([0.01, 1])).value(grid)
#
# seasons = (tokensPool.Sin(params=[0.3, 1/(2*np.pi), 0]).value(grid) +
#            tokensPool.Imp(params=np.array([1, 0.5 / (2 * np.pi), 0.9, 0.5, 0, 0, 0])).value(grid))
# ts = trend + seasons
# ts -= ts.mean()
#
# freqs, spec = fp.fft(grid, ts, 0, 1)
# spec = np.abs(spec)
#
# axs[0].plot(grid, ts)
# axs[0].set_xlabel('t, s')
# axs[0].set_ylabel('Series')
# axs[0].grid(True)
#
# axs[1].plot(2*np.pi*freqs, spec)
# axs[1].set_xlabel('freq, Hz')
# axs[1].set_ylabel('Spectra')
# axs[1].grid(True)
# axs[1].annotate('trend', xy=(0.044, 218.16), xytext=(2, 218.16),
#                 arrowprops=dict(facecolor='black', shrink=0,
#                                 width=0.1, headwidth=2))
# axs[1].annotate('sharp pulses', xy=(0.5, 99), xytext=(1.5, 180),
#                 arrowprops=dict(facecolor='black', shrink=0,
#                                 width=0.1, headwidth=2))
# axs[1].annotate('sine', xy=(1, 111), xytext=(2.5, 150),
#                 arrowprops=dict(facecolor='black', shrink=0,
#                                 width=0.1, headwidth=2))


plt.tight_layout()
plt.show()



