"""
Contains inheritors/implementations of baseline classes for Individual.
"""
from copy import copy, deepcopy
from functools import reduce
from buildingBlocks.baseline.Individs import Individ
import numpy as np


class Equation(Individ):
    """
    An expression that approximates the input data and is used as a basis for building synthetics.
    """
    def __init__(self, genetic_operators: dict = None, chromo: list = None,
                 kind: str = 'init', used_value: str = 'plus', forms=None):
        super().__init__(genetic_operators, chromo)
        if forms is None:
            forms = []
        self.elitism = False
        self.selected = False

        # self.kind = kind
        self.used_value = used_value
        self.forms = forms

    def copy(self):
        new_copy = copy(self)
        new_copy.chromo = deepcopy(new_copy.chromo)
        new_copy.fixes = deepcopy(new_copy.fixes)

        try:
            new_copy.forms = deepcopy(new_copy.forms)
        except:
            pass

        return new_copy

    def formula(self, with_params=False):
        if self.used_value == "plus":
            joinWith = '+'
        else:
            joinWith = '*'
        return joinWith.join(list(map(lambda x: x.name(with_params), self.chromo)))

    def name(self, with_params=True):
        s = type(self).__name__ + '('
        s += 'chromo=['
        for gen in self.chromo:
            s += gen.name(with_params)
            s += ','
        s = s[:-1]
        s += '], used_value="' + self.used_value + '")'
        return s

    def value(self, grid):
        if len(self.chromo) != 0:
            # if self.used_value == 'plus':
            return reduce(lambda val, x: val + x, list(map(lambda x: x.value(grid), self.chromo)))
        #     elif self.used_value == 'product':
        #         return reduce(lambda val, x: val * x, list(map(lambda x: x.value(t), self.chromo)))
        return np.zeros(grid.shape)

    def get_chromo(self, types=None):
        if types is None:
            return copy(self.chromo)
        return list(filter(lambda x: x.type in types, self.chromo))

    # del cache before sending object to another process
    # TODO do it for back sending by changing magic methods
    def send_preparing(self):
        for token in self.chromo:
            token.val = None
