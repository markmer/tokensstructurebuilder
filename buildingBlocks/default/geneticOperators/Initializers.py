from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation


class InitIndivid(GeneticOperatorIndivid):
    def __init__(self, params=None):
        super().__init__(params=params)

    def apply(self, individ, *args, **kwargs) -> None:
        # len_formula = np.random.randint(1, self.params['max_len'] + 1)
        # individ.genetic_operators['mutation']()
        individ.apply_operator('mutation')


class InitPopulation(GeneticOperatorPopulation):
    def __init__(self, params):
        super().__init__(params=params)

    def apply(self, population, *args, **kwargs) -> list:
        population = []
        for _ in range(self.params['population_size']):
            new_individ = self.params['individ'].copy()
            new_individ.apply_operator('initialize')
            population.append(new_individ)
        return population
