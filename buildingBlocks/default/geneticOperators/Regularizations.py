"""
Contains Genetic Operators responsible for length of individ's chromosomes.
The chromosome can be either shortened or expanded.

Classes
----------
LassoIndivid
DEOptIndivid
DelDuplicateTokensIndivid
CheckMandatoryTokensIndivid

LassoPopulation
RegularisationPopulation
"""
import random
from scipy.optimize import differential_evolution
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.preprocessing import normalize, scale
from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation
from buildingBlocks.baseline.Tokens import ComplexToken
import numpy as np
from multiprocessing import current_process


class LassoIndivid(GeneticOperatorIndivid): #TODO не удалять токены а повысить шанс их мутации/кроссовера/ухода из хромосомы
    def __init__(self, params):
        super().__init__(params=params)

    def _preprocessing_data(self, individ, normalize_=True):
        """
        Нормирует все данные, выбирает наиболее значимую фичу по норме и представляет ее в качестве таргета
        Args:
            individ:

        Returns:

        """
        chromo = individ.get_chromo()
        features = np.transpose(np.array(list(map(lambda token: token.value(self.params['grid']), chromo))))
        if normalize_:
            # features -= np.mean(features, axis=0, keepdims=True)
            # features, norms = normalize(features, norm='l2', axis=0, return_norm=True)

            mandatory_idxs = [idx for idx in range(len(chromo)) if chromo[idx].mandatory != 0]
            if len(mandatory_idxs) == 1:
                target_idx = mandatory_idxs[0]
            elif len(mandatory_idxs) == 2:
                cov = np.cov(features.T[mandatory_idxs])
                idx = np.argmax([cov[i][i] for i in range(2)])
                target_idx = mandatory_idxs[idx]
            else:
                if len(mandatory_idxs) == 0:
                    # target_idx = np.random.randint(0, len(chromo))
                    new_features = features.T
                    mandatory_idxs = [idx for idx in range(len(chromo))]
                else:
                    # target_idx = np.random.choice(mandatory_idxs)
                    new_features = features.T[mandatory_idxs]
                cov = np.abs(np.corrcoef(new_features))
                for i in range(len(cov)):
                    cov[i][i] = 0
                x, y = np.unravel_index(cov.argmax(), cov.shape)
                target_idx = mandatory_idxs[np.random.choice((x, y))]

                # target_idx = mandatory_idxs[np.argmax(cov.sum(axis=1))]
            # print('lasso target idx--->', target_idx, ' ', individ.chromo[target_idx].name())
        else:
            # target_idx = np.argmax(np.var(features, axis=0))
            # target_idx = np.random.randint(features.shape[1])
            if features.T.shape[0] >= 3:
                target_idx = 0
            else:
                cov = np.cov(features.T)
                target_idx = np.argmax([cov[i][i] for i in range(2)])
            # print('LR target idx--->', target_idx, ' ', individ.chromo[target_idx].name())
        target = -features[:, target_idx]
        idxs = [i for i in range(features.shape[1]) if i != target_idx]
        features = features[:, idxs]
        return features, target, target_idx

    def _regression_cascade(self, individ, lasso=True):
        features = np.array(list(map(lambda token: token.value(self.params['grid']), individ.chromo)))
        features -= np.mean(features, axis=1, keepdims=True)
        features, norms = normalize(features, norm='l2', axis=1, return_norm=True)

        models = []
        for idx in range(len(features)):
            if individ.chromo[idx].mandatory == 0:
                continue
            target = features[idx]
            X = features[[i for i in range(len(features)) if i != idx]].T
            if lasso:
                model = Lasso(self.params['regularisation_coef'])
            else:
                model = LinearRegression()
            model.fit(X, target)
            models.append((idx, model, model.score(X, target)))
        print([model[2] for model in models])
        models.sort(key=lambda model: -model[2])
        return models[0]

    @staticmethod
    def _set_amplitudes_after_regression(individ, coefs, target_idx):  # , norms):
        chromo = individ.get_chromo()
        chromo.pop(target_idx)
        for idx, token in enumerate(chromo):
            token.set_param(token.param(name='Amplitude') * coefs[idx], name='Amplitude')  # /norms[idx])

    @staticmethod
    def _del_tokens_with_zero_coef(individ, coefs, target_idx):
        print('lasso target idx--->', target_idx, ' ', individ.chromo[target_idx].name(), coefs)
        new_chromo = [individ.chromo.pop(target_idx)]
        # if (coefs == 0).all():
        #     individ.chromo.reverse()
        #     individ.chromo.extend(new_chromo)
        #     individ.chromo.reverse()
        #     return
        for idx, coef in enumerate(coefs):
            if coef != 0 or individ.chromo[idx].mandatory != 0: #or (individ.chromo[idx].mandatory == 0
                                                                 #  and not individ.chromo[idx].fix):
                new_chromo.append(individ.chromo[idx])
        idxs = [i for i in range(len(coefs))]
        random.shuffle(idxs)
        for idx in idxs:
            if coefs[idx] == 0 and len(new_chromo) < 2 and individ.chromo[idx] not in new_chromo:
                new_chromo.append(individ.chromo[idx])
        individ.chromo = new_chromo

    def linear_regression(self, individ):
        if len(individ.chromo) <= 1:
            return
        model = LinearRegression(fit_intercept=True)
        features, target, target_idx = self._preprocessing_data(individ, normalize_=False)
        model.fit(features, target)
        self._set_amplitudes_after_regression(individ, model.coef_, target_idx)
        # print(model.intercept_, model.coef_)

        try:
            individ.forms.append('LR-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass

    def lasso(self, individ, lasso=True):
        if len(individ.chromo) <= 2:
            return
        # model = Lasso(self.params['regularisation_coef'])
        # features, target, target_idx = self._preprocessing_data(individ)
        # model.fit(features, target)
        target_idx, model, _ = self._regression_cascade(individ, lasso=lasso)
        if lasso:
            self._del_tokens_with_zero_coef(individ, model.coef_, target_idx)
        # print(model.intercept_, model.coef_)

        try:
            individ.forms.append('lasso-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass

    def apply(self, individ, *args, **kwargs):
        try:
            fix = individ.fixes['lasso']
        except:
            individ.fixes['lasso'] = False
            fix = False
        if fix:
            return
        try:
            lasso = kwargs['use_lasso']
        except:
            lasso = True
        self.lasso(individ, lasso)
        self.linear_regression(individ)
        # self.lasso(individ)
        individ.fixes['lasso'] = lasso


class DEOptIndivid(GeneticOperatorIndivid):

    def __init__(self, params):
        super().__init__(params=params)

    def _preprocessing_data(self, individ):
        chromo = individ.get_chromo()
        features = np.array(list(map(lambda token: token.value(self.params['grid']), chromo)))
        return features

    @staticmethod
    def _set_amplitudes_after_regression(individ, coefs):
        chromo = individ.get_chromo()
        for idx, token in enumerate(chromo):
            token.set_param(token.param(name='Amplitude') * coefs[idx], name='Amplitude')

    @staticmethod
    def Q(w, *args):
        return np.linalg.norm(w @ args[0]) + args[1] * (1 / np.abs(w).sum() + np.abs(w).sum())

    def apply(self, individ, *args, **kwargs) -> None:
        X = self._preprocessing_data(individ)
        bounds = [(-1, 1) for i in range(X.shape[0])]
        res = differential_evolution(self.Q, bounds, args=(X, 1), popsize=3)
        self._set_amplitudes_after_regression(individ, res.x)

        try:
            individ.forms.append('DE-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass


class DelDuplicateTokensIndivid(GeneticOperatorIndivid):
    """
    Del all equivalent tokens in Individ's chromosome.
    """
    def __init__(self, params=None):
        super().__init__(params=params)

    def apply(self, individ, *args, **kwargs):
        try:
            fix = individ.fixes['DelDuplicate']
        except:
            individ.fixes['DelDuplicate'] = False
            fix = False
        if fix:
            return
        # individ.chromo.sort(key=lambda token: -abs(token.param(name='Amplitude')))
        new_chromo = []
        for token in individ.chromo:
            token.set_param(1., name='Amplitude')
            if token not in new_chromo:
                new_chromo.append(token)
        individ.chromo = new_chromo
        if len(individ.chromo) < 2:
            individ.apply_operator(name='mutation')
        individ.fixes['DelDuplicate'] = True

        try:
            individ.forms.append('DelDuplicate-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass


class CheckMandatoryTokensIndivid(GeneticOperatorIndivid):
    """
    Checking for the presence of required tokens in the chromosome and adding them.
    """
    def __init__(self, params=None):
        super().__init__(params=params)

    def _individ_mandatories(self, individ):
        mandatories = set()
        for token in individ.chromo:
            if isinstance(token, ComplexToken):
                for subtoken in token.subtokens:
                    mandatories.add(subtoken.mandatory)
            else:
                mandatories.add(token.mandatory)
        return mandatories

    def _check_missing_mandatory_tokens_in(self, individ):
        individ_mandatories = self._individ_mandatories(individ)
        mandatories = set(map(lambda token: token.mandatory, self.params['tokens']))
        individ_mandatories.discard(0)
        mandatories.discard(0)
        diff = mandatories.difference(individ_mandatories)
        return list(filter(lambda token: token.mandatory in diff, self.params['tokens']))

    def apply(self, individ, *args, **kwargs):
        missing_tokens = self._check_missing_mandatory_tokens_in(individ)
        if not missing_tokens:
            return
        terminal_tokens = list(filter(lambda token: not isinstance(token, ComplexToken), individ.chromo))
        complex_tokens = list(filter(lambda token: isinstance(token, ComplexToken), individ.chromo))
        for token in missing_tokens:
            # if terminal_tokens and complex_tokens:
            #     if np.random.uniform() < self.params['add_to_complex_prob']:
            #         complex_token = np.random.choice(complex_tokens)
            #         complex_token.add_subtoken(token.copy())
            #     else:
            #         individ.add_tokens(token.copy())
            # elif complex_tokens:
            #     complex_token = np.random.choice(complex_tokens)
            #     complex_token.add_subtoken(token.copy())
            # else:
            individ.add_tokens(token.copy())

        try:
            individ.forms.append('CheckMandatory-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass


class LassoPopulation(GeneticOperatorPopulation):
    def __init__(self, params=None):
        super().__init__(params=params)

    def apply(self, population, *args, **kwargs) -> list:
        for individ in population:
            individ.apply_operator('LASSO')
        return population


class RegularisationPopulation(GeneticOperatorPopulation):
    def __init__(self, params=None):
        super().__init__(params=params)

    def apply(self, population, *args, **kwargs) -> list:
        for individ in population:
            individ.apply_operator('DelDuplicate')
            individ.apply_operator('CheckMandatory')
            # individ.apply_operator('LASSO')
        return population
