from multiprocessing import current_process

from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation
import numpy as np


class CrossoverIndivid(GeneticOperatorIndivid):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('cross_intensive', 'increase_prob')

    def apply(self, individ, *args, **kwargs): #TODO: не добавлять заведомо присутствующие токены
        cross_intensive = self.params['cross_intensive']
        increase_prob = self.params['increase_prob']
        # ind1 = individs[0].copy() изменяются сами
        # ind2 = individs[1].copy()

        ind1 = individ
        ind2 = kwargs['other_individ']

        # ind1.kind += '->crossover'
        # ind2.kind += '->crossover'

        # inds change tokens or add them to each other depending on increase prob
        # tokens remain fixed!
        cross_intensive = np.random.randint(1, 1 + np.min([cross_intensive,
                                                           len(ind1.chromo), len(ind2.chromo)]))
        add_idxs1, add_idxs2 = tuple(map(lambda ind: np.random.choice(len(ind.chromo), size=cross_intensive,
                                                                      replace=False),
                                         (ind1, ind2)))

        # add_idxs1 = np.random.choice(len(ind1.chromo), size=cross_intensive, replace=False)
        # add_idxs2 = np.random.choice(len(ind2.chromo), size=cross_intensive, replace=False)
        if np.random.uniform() < increase_prob:
            for idx in add_idxs1:
                token = ind1.chromo[idx].copy()
                # token.set_param(1., name='Amplitude')
                ind2.add_tokens(token)
            for idx in add_idxs2:
                token = ind2.chromo[idx].copy()
                token.set_param(1., name='Amplitude')
                ind1.add_tokens(token)
        else:
            for idx1, idx2 in np.transpose([add_idxs1, add_idxs2]):
                # ind1.chromo[idx1], ind2.chromo[idx2] = ind2.chromo[idx2], ind1.chromo[idx1]
                token1, token2 = ind1.chromo[idx1], ind2.chromo[idx2]
                token1.set_param(1., name='Amplitude')
                token2.set_param(1., name='Amplitude')
                ind1.set_token(token2, idx1)
                ind2.set_token(token1, idx2)
        # return [ind1, ind2]
        try:
            for ind in (ind1, ind2):
                ind.forms.append('crossover-->' + ind.formula() + '<---' + current_process().name)
        except:
            pass


class CrossoverPopulation(GeneticOperatorPopulation):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('crossover_size')

    def apply(self, population, *args, **kwargs):
        selected_population = list(filter(lambda individ: individ.selected, population))
        crossover_size = self.params['crossover_size']
        if crossover_size is None:
            selected_individs = selected_population
        else:
            assert crossover_size <= len(selected_population), "Crossover size in pairs" \
                                                               " must be less than population size"
            selected_individs = np.random.choice(selected_population, replace=True, size=(crossover_size, 2))

        for individ1, individ2 in selected_individs:
            for individ in individ1, individ2:
                if individ.elitism:
                    individ.elitism = False
                    new_individ = individ.copy()
                    new_individ.selected = False
                    population.append(new_individ)
            individ1.apply_operator('crossover', other_individ=individ2)  # Параметры мутации заключены в операторе мутации с которым
        return population
