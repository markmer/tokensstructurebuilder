from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation
from functools import reduce
import numpy as np


class MSEFitnessIndivid(GeneticOperatorIndivid):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('grid')

    def apply(self, individ, *args, **kwargs) -> None:
        try:
            fix = individ.fixes['fitness']
        except:
            individ.fixes['fitness'] = False
            fix = False
        if fix and individ.fitness is not None:
            return
        vec = reduce(lambda val, token: val + token,
                     list(map(lambda token: token.value(self.params['grid']), individ.chromo)))
        # individ.fitness = np.linalg.norm(vec)
        individ.fitness = np.var(vec)
        individ.fixes['fitness'] = True


class FitnessPopulation(GeneticOperatorPopulation):
    def __init__(self, params=None):
        super().__init__(params=params)

    def apply(self, population, *args, **kwargs) -> list:
        for individ in population:
            individ.apply_operator('fitness')
        return population
