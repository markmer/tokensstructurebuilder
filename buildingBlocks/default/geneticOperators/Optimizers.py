"""
Contains optimizers of parameters of tokens which are need to optimize.
Лучше сюда не заглядывать, тут есть и будут еще сложные логики оптимизации токенов.
"""
from functools import reduce

from numba import jit

from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation
from buildingBlocks.default.geneticOperators._supplementary.FrequencyProcessor import FrequencyProcessor4TimeSeries as fp
from scipy.optimize import differential_evolution, minimize
import numpy as np
from copy import deepcopy
from multiprocessing import current_process


class PeriodicTokensOptimizerIndivid(GeneticOperatorIndivid):
    """
    Works with periodic simple token objects of the 'Function'class in Individ chromosome.
    Optimizes the parameters of the token for better approximation of input data.
    """

    def __init__(self, params=None):
        if params is None:
            params = {}
        add_params = {
            'optimizer': 'DE',
            'optimize_id': None,
            'popsize': 7,
            'eps': 0.005
        }
        for key, value in add_params.items():
            if key not in params.keys():
                params[key] = value
        super().__init__(params=params)
        self._check_params('grid', 'optimizer', 'optimize_id', 'popsize', 'eps')

    @staticmethod
    def _fitness_wrapper(params, *args):
        individ, grid, token = args
        token.params = params
        individ.fitness = None
        individ.apply_operator(name='fitness')
        return individ.fitness

    def _optimize_token_params(self, individ, token):
        grid = self.params['grid']
        individ.apply_operator(name='LASSO', use_lasso=False)
        target = -(individ.value(grid) - token.value(grid))
        target -= target.mean()
        target /= np.abs(target).max()
        freq = fp.choice_freq_for_summand(grid, target, max_len=2, choice_size=2, token_type='seasonal')
        if freq is None: #TODO: сделать проверку присутствия нужного токена в неком пуле, чтобы избежать повторной оптимизаци
            individ.chromo.remove(token) # del hopeless token and out
        else:
            eps = self.params['eps']
            bounds = deepcopy(token.get_descriptor_foreach_param(descriptor_name='bounds'))
            bounds[token.get_key_use_params_description(descriptor_name='name',
                                                        descriptor_value='Frequency')] = (freq * (1 - eps), freq * (1 + eps))
            x0 = deepcopy(token.params)
            x0[token.get_key_use_params_description(descriptor_name='name',
                                                    descriptor_value='Frequency')] = freq
            if self.params['optimizer'] == 'DE':
                res = differential_evolution(self._fitness_wrapper, bounds,
                                             args=(individ, grid, token),
                                             popsize=self.params['popsize'])
            else:
                res = minimize(self._fitness_wrapper,  x0,
                               args=(individ, grid, token))
            # print(self._fitness_wrapper([1, 1, 1], individ, grid, target, token))
            token.params = res.x
            token.fix = True
        individ.chromo = individ.chromo

        try:
            individ.forms.append('PeriodicOptimize-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass

    def _choice_tokens_for_optimize(self, individ):
        optimize_id = self.params['optimize_id']
        choiced_tokens = list(filter(lambda token: token.optimize_id == optimize_id and not token.fix,
                                     individ.chromo))
        # Зануляем амплитуды токенов чтобы не мешали оптимизирововать друг друга по порядку
        for token in choiced_tokens:
            token.set_param(0., name='Amplitude')
        return choiced_tokens

    def apply(self, individ, *args, **kwargs) -> None:
        try:
            fix = individ.fixes['optimize']
        except:
            individ.fixes['optimize'] = False
            fix = False
        if fix:
            return
        choiced_tokens = self._choice_tokens_for_optimize(individ)
        for token in choiced_tokens:
            self._optimize_token_params(individ, token)
        individ.fixes['optimize'] = True


class PeriodicInProductTokensOptimizerIndivid(GeneticOperatorIndivid):
    """
    Works with periodic simple token objects of the 'Function'class in Individ chromosome.
    Optimizes the parameters of the token for better approximation of input data.
    """

    def __init__(self, params=None):
        if params is None:
            params = {}
        add_params = {
            'optimizer': 'DE',
            'optimize_id': None,
            'popsize': 7,
            'eps': 0.005
        }
        for key, value in add_params.items():
            if key not in params.keys():
                params[key] = value
        super().__init__(params=params)
        self._check_params('grid', 'optimizer', 'optimize_id', 'popsize', 'eps')

    @staticmethod
    def _fitness_wrapper(params, *args):
        individ, grid, tokens = args
        params = np.array_split(params, len(tokens))
        for idx, token in enumerate(tokens):
            params_idxs = [i for i in range(len(token.params)) if i !=
                           token.get_key_use_params_description(descriptor_name='name',
                                                                descriptor_value='Frequency')]
            token.params[params_idxs] = params[idx]
            token._fix_val = False
            # token.params = params[idx]
            # if idx != 0:
            #     token.set_param(1, name='Amplitude') #TODO: Надо что то сделать чтобы не занулялись амплитуды при поиске
        # для пересчета фитнеса индивида
        individ.fitness = None
        individ.apply_operator(name='fitness')
        return individ.fitness #TODO: нужно сверху наложить штраф на маленькие амплитуды для использования ДЕ

    def _preprocessing_product_tokens(self, individ):
        grid = self.params['grid']
        choiced_subtokens, complex_tokens_with_choiced_subtokens = self._choice_tokens_for_optimize(
            individ, ret_complex_tokens_with_choiced_subtokens=True)
        if len(choiced_subtokens) == 1:
            assert len(complex_tokens_with_choiced_subtokens) == 1
            current_complex_token = complex_tokens_with_choiced_subtokens[0]
            choiced_subtoken = choiced_subtokens[0]
            target_chromo = list(filter(lambda token: token != current_complex_token and token.mandatory != 0,
                                        individ.chromo))
            #TODO: можно заменить проверку на token.fix
            current_complex_token_fix_subtokens = list(filter(lambda token: token != choiced_subtoken,
                                                              current_complex_token.subtokens))
            current_complex_token_value = current_complex_token.param(name='Amplitude')*reduce(
                lambda val, x: val * x, list(map(lambda x: x.value(grid), current_complex_token_fix_subtokens)))
            current_complex_token_freqs = fp.find_freq_for_summand(grid, current_complex_token_value,
                                                                   max_len=len(current_complex_token_fix_subtokens),
                                                                   choice_size=len(current_complex_token_fix_subtokens))
            optimization_info = []
            for target_idx, target_token in enumerate(target_chromo):
                target = -target_token.value(grid)
                recommended_freqs = fp.find_freq_for_multiplier(grid, target,
                                                                current_complex_token_freqs, max_len=10)
                if len(recommended_freqs) == 0:
                    continue
                for recommended_freq in recommended_freqs:
                    optimization_info.append([target_idx, target_token, *recommended_freq])

            if len(optimization_info) == 0:
                current_complex_token.del_subtoken(choiced_subtoken)
                return

            optimization_case = optimization_info[np.random.randint(len(optimization_info))]
            target_idx, target_token, current_complex_token_freq, lower_freq, higher_freq, diff_freq = optimization_case
            W_max = 1/np.mean(grid[1:] - grid[:-1])
            # если нижняя разностная частота уходит в ноль, значит несущая частота и разностная частота равны,
            # и мы должны домножить токен-случай с таким сходством на выбранный токен, и оптимизировать фазы
            if lower_freq <= 0.01*W_max:
                if type(target_token) in self.params['complex_tokens_types']:
                    target_token.add_token(choiced_subtoken.copy())
                else:
                    new_complex_token = type(current_complex_token)()
                    new_complex_token.subtokens = [target_token, choiced_subtoken.copy()]
                    individ.chromo[individ.chromo.index(target_token)] = new_complex_token
                return current_complex_token_freq

            return diff_freq
        else:
            assert len(choiced_subtokens) == 1

    def _optimize_token_params(self, individ, tokens: list):
        grid = self.params['grid']
        individ.apply_operator(name='LASSO', use_lasso=False)
        freq = self._preprocessing_product_tokens(individ)
        if freq is None: #TODO: сделать проверку присутствия нужного токена в неком пуле, чтобы избежать повторной оптимизаци
            for token in tokens:
                for ind_token in individ.chromo:
                    try:
                        ind_token.del_subtoken(token)
                    except:
                        pass
        else:
            tokens, complex_tokens_with_choiced_subtokens = self._choice_tokens_for_optimize(individ,
                                                                                             ret_complex_tokens_with_choiced_subtokens=True)
            for token in complex_tokens_with_choiced_subtokens:
                token.cache_val = False
            eps = self.params['eps']
            bounds = []
            x0 = []
            for token in tokens:
                token.set_param(freq, name='Frequency')
                token.set_param(1., name='Amplitude') #TODO: убрать из оптимизируемых параметров частоту и одну из амплитуд
                token.set_param(0., name='Phase')

            tokens = tokens[:1]
            for token in tokens:
                bounds_token = deepcopy(token.get_descriptor_foreach_param(descriptor_name='bounds'))
                # bounds_token[token.get_key_use_params_description(descriptor_name='name',
                #                                                   descriptor_value='Frequency')] = (freq * (1 - eps),
                #                                                                                     freq * (1 + eps))
                del bounds_token[token.get_key_use_params_description(descriptor_name='name',
                                                                      descriptor_value='Frequency')]
                bounds.extend(bounds_token)

                x0_token = list(deepcopy(token.params))
                # x0_token[token.get_key_use_params_description(descriptor_name='name',
                #                                               descriptor_value='Frequency')] = freq
                del x0_token[token.get_key_use_params_description(descriptor_name='name',
                                                                  descriptor_value='Frequency')]
                x0.extend(x0_token)

            x0 = np.array(x0)

            if self.params['optimizer'] == 'DE':
                res = differential_evolution(self._fitness_wrapper, bounds,
                                             args=(individ, grid, tokens),
                                             popsize=self.params['popsize'])
            else:
                res = minimize(self._fitness_wrapper,  x0,
                               args=(individ, grid, tokens))

            print(res)
            answer = np.array_split(res.x, len(tokens))
            for idx, token in enumerate(tokens):
                params_idxs = [i for i in range(len(token.params)) if i !=
                               token.get_key_use_params_description(descriptor_name='name',
                                                                    descriptor_value='Frequency')]
                token.params[params_idxs] = answer[idx]
                token._fix_val = False
                # token.params = answer[idx]
                token.fix = True
            for token in complex_tokens_with_choiced_subtokens:
                token.cache_val = True
        individ.chromo = individ.chromo

        try:
            individ.forms.append('PeriodicOptimize-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass

    def _choice_tokens_for_optimize(self, individ, ret_complex_tokens_with_choiced_subtokens=False):
        optimize_id = self.params['optimize_id']
        complex_tokens_in_chromo = list(filter(lambda token: type(token) in self.params['complex_tokens_types'],
                                                individ.chromo))
        choiced_subtokens = list(filter(lambda subtoken: subtoken.optimize_id == optimize_id and not subtoken.fix,
                                     [subtoken for token in complex_tokens_in_chromo
                                      for subtoken in token.subtokens]))
        # Зануляем амплитуды токенов чтобы не мешали оптимизирововать друг друга по порядку
        for token in choiced_subtokens:
            token.set_param(0., name='Amplitude')
        if ret_complex_tokens_with_choiced_subtokens:
            complex_tokens_with_choiced_subtokens = []
            for token in complex_tokens_in_chromo:
                for subtoken in token.subtokens:
                    if subtoken in choiced_subtokens:
                        complex_tokens_with_choiced_subtokens.append(token)
            # complex_tokens_with_choiced_subtokens = list(filter(lambda token:
            #                                                     not set(token.subtokens).isdisjoint(
            #                                                         set(choiced_subtokens)),
            #                                                     complex_tokens_in_chromo))
            return choiced_subtokens, complex_tokens_with_choiced_subtokens
        return choiced_subtokens

    def apply(self, individ, *args, **kwargs) -> None:
        try:
            fix = individ.fixes['optimize_product']
        except:
            individ.fixes['optimize_product'] = False
            fix = False
        if fix:
            return
        choiced_tokens = self._choice_tokens_for_optimize(individ)
        self._optimize_token_params(individ, tokens=choiced_tokens)
        individ.fixes['optimize_product'] = True


class PeriodicTokensOptimizerPopulation(GeneticOperatorPopulation):
    def __init__(self, params=None):
        super().__init__(params=params)

    def apply(self, population, *args, **kwargs):
        for individ in population:
            individ.apply_operator('optimize')
        return population



