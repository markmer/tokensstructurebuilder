from buildingBlocks.baseline.GeneticOperators import GeneticOperatorPopulation
import numpy as np


class Elitism(GeneticOperatorPopulation):
    """
    Marks the given number of the best individs. Change property self.elitism.
    """
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('elitism')

    def apply(self, population, *args, **kwargs):
        for individ in population:
            individ.elitism = False
        elite_idxs = np.argsort(list(map(lambda individ: individ.fitness,
                                         population)))[:self.params['elitism']]
        for idx in elite_idxs:
            population[idx].elitism = True
        return population


class RouletteWheelSelection(GeneticOperatorPopulation):
    """
    Marks the given number of the selected individs. Change property self.selected.
    """
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('tournament_size', 'winners_size')

    def apply(self, population: list, *args, **kwargs) -> list:
        pops = list(filter(lambda individ: len(individ.chromo) > 1, population)) # может быть [] (так как удаляются
        # все не мандатори токены, например периодические если не было найдено нормальной частоты)
        assert len(pops) != 0, 'All individs in population consist of one token (probably because ' \
                               'all non-mandatory tokens were deleted during optimization or regularization)'
        for individ in pops:
            individ.selected = False

        tournament_size = self.params['tournament_size']
        winners_size = self.params['winners_size']
        if tournament_size is None:
            tournament_size = len(pops)
            selected_individs = pops
        else:
            # assert tournament_size <= len(pops), "Tournament size must be less than population size"
            tournament_size = min(tournament_size, len(pops))
            selected_individs = np.random.choice(pops, replace=False, size=tournament_size)
        population_fitnesses = list(map(lambda individ: 1/(individ.fitness +0.01), selected_individs))
        fits_sum = np.sum(population_fitnesses)
        probabilities = list(map(lambda x: x/fits_sum, population_fitnesses))
        if winners_size is None:
            winners = selected_individs
        else:
            assert tournament_size >= winners_size, "Winners size must be less than tornament size"
            winners = np.random.choice(selected_individs, size=winners_size, p=probabilities, replace=False)
        # return list(winners)
        for individ in winners:
            individ.selected = True
        return population


class RestrictPopulation(GeneticOperatorPopulation):
    """
    Restrict the size of the population to the given population_size according to the fitness-dependent probability.
    """
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('population_size')

    def apply(self, population, *args, **kwargs):
        if self.params['population_size'] < len(population):
            elite = list(filter(lambda individ: individ.elitism == True, population))
            # Исключаем элиту чтобы позже ее добавить
            for individ in elite:
                population.remove(individ)
            population_fitnesses = list(map(lambda individ: 1/(individ.fitness+0.01), population))
            fits_sum = np.sum(population_fitnesses)
            probabilities = list(map(lambda x: x / fits_sum, population_fitnesses))
            population = list(np.random.choice(population, size=self.params['population_size'],
                                               p=probabilities, replace=False))
            population.extend(elite)
        return population


