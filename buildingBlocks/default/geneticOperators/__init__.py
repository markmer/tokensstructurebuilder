"""
This package contains default implementations of different Genetic Operators.
For example Crossover, Mutation, FitnessEvaluator, Selectors etc. for work with individs and population.
"""