import random
from multiprocessing import current_process
from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation
from buildingBlocks.baseline.Tokens import ComplexToken
import numpy as np


class MutationIndivid(GeneticOperatorIndivid):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('mut_intensive', 'increase_prob', 'tokens')

    def apply(self, individ, *args, **kwargs) -> None:
        # individ.kind += '->mutation'
        tokens = list(filter(lambda token: token not in individ.chromo, self.params['tokens']))
        if not tokens:
            return
        # define how many tokens will be added
        mut_intensive = np.random.randint(1, 1 + min(self.params['mut_intensive'],
                                                     len(tokens)))
        if len(individ.chromo) == 0:
            mut_intensive = 2

        add_tokens = list(np.random.choice(tokens, size=mut_intensive, replace=False))
        #TODO: добавить выборщик токена в зависимости от текущего вида индивида
        for idx, token in enumerate(add_tokens):
            add_tokens[idx] = token.copy()
        # tokens is added to the chromo/ replace tokens in chromo/ both variants
        if np.random.uniform() < self.params['increase_prob']:
            individ.add_tokens(add_tokens)
        else:
            for idx in np.random.choice(range(len(individ.chromo)),
                                        size=min(len(individ.chromo), len(add_tokens)), replace=False):
                individ.set_token(add_tokens.pop(), idx)
            # if len(add_functions) > len(chromo) we add token to the chromo
            if add_tokens:
                individ.add_tokens(add_tokens)

        try:
            individ.forms.append('mutation-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass


class ProductTokenMutationIndivid(GeneticOperatorIndivid):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('mut_prob', 'tokens', 'product_token', 'max_multi_len')

    def apply(self, individ, *args, **kwargs) -> None:
        tokens = self.params['tokens']
        if len(tokens) == 0 or np.random.uniform() < self.params['mut_prob']:
            return
        add_token = np.random.choice(tokens,).copy()
        product_token = self.params['product_token'].copy()
        # idx = np.random.randint(0, len(ind.chromo))
        idxs = [i for i in range(len(individ.chromo))]
        random.shuffle(idxs)
        # TODO: make loop for add_tokens if len(add_tokens) > 1
        for idx in idxs:
            if type(individ.chromo[idx]).__name__ == type(product_token).__name__:
                ind_chromo_token_sub_len = len(individ.chromo[idx].subtokens)
                if (ind_chromo_token_sub_len < self.params['max_multi_len']
                        and np.random.uniform() < 0.5):
                    individ.chromo[idx].add_subtoken(add_token)
                    if individ.chromo.count(individ.chromo[idx]) > 1:
                        individ.chromo[idx].del_subtoken(add_token)
                    else:
                        break
                else:
                    token_idxs = [i for i in range(ind_chromo_token_sub_len)]
                    random.shuffle(token_idxs)
                    for token_idx in token_idxs:
                        old_subtoken = individ.chromo[idx].subtokens[token_idx]
                        individ.chromo[idx].set_subtoken(add_token, idx=token_idx)
                        if individ.chromo.count(individ.chromo[idx]) > 1:
                            individ.chromo[idx].set_subtoken(old_subtoken, idx=token_idx)
                            flag = False
                        else:
                            flag = True
                            break
                        if flag:
                            break
            elif individ.chromo[idx].name_ in list(map(lambda token: token.name_,
                                                   tokens)) and not isinstance(individ.chromo[idx], ComplexToken): # если токен не является сложным ( в частности продуктом), но в списке простых токенов
                if self.params['max_multi_len'] <= 1:
                    return
                new_product_token = product_token
                new_product_token.subtokens = [individ.chromo[idx], add_token]
                if individ.chromo.count(new_product_token) == 0:
                    individ.chromo[idx] = new_product_token
                    assert len(individ.chromo[idx].subtokens) <= self.params['max_multi_len']
                    break
        individ.change_all_fixes(False)

        try:
            individ.forms.append('product_mutation-->' + individ.formula() + '<---' + current_process().name)
        except:
            pass


class MutationPopulation(GeneticOperatorPopulation):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('mutation_size')

    def apply(self, population, *args, **kwargs) -> list:
        selected_population = list(filter(lambda individ: individ.selected, population))
        mutation_size = self.params['mutation_size']
        if mutation_size is None:
            selected_individs = selected_population
        else:
            assert mutation_size <= len(selected_population), "Mutations size must be less than population size"
            selected_individs = np.random.choice(selected_population, replace=False, size=mutation_size)

        for individ in selected_individs:
            if individ.elitism:
                individ.elitism = False
                new_individ = individ.copy()
                new_individ.selected = False
                population.append(new_individ)
            individ.apply_operator('mutation')
            individ.apply_operator('product mutation')
        return population

