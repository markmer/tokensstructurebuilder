"""
Contains Operators Map that maps names of genetic operators and their implementation.
"""
from buildingBlocks.baseline.GeneticOperators import GeneticOperator, OperatorsMapping as om
from dataclasses import dataclass


# Наследуемся от базового датакласса (ведущего себя как словарь), и добавляем дополнительные обязательные поля
# к уже имеющимся
@dataclass
class OperatorsMapping(om):
    lassoIndivid: GeneticOperator = GeneticOperator()
    lassoPopulation: GeneticOperator = GeneticOperator()
    delDuplicateTokensIndivid: GeneticOperator = GeneticOperator()
    checkMandatoryTokensIndivid: GeneticOperator = GeneticOperator()
    regularisationPopulation: GeneticOperator = GeneticOperator()
    elitismPopulation: GeneticOperator = GeneticOperator()
    restrictionPopulation: GeneticOperator = GeneticOperator()
    productMutationIndivid: GeneticOperator = GeneticOperator()


class OperatorsMapKeeper:
    """
    Class for keeping object of class 'OperatorsMapping'.
    """
    keep = None


keeper = OperatorsMapKeeper()


def set_operators_mapper(operatorsMap_: om):
    """
    Set the property keeper for object of class 'OperatorsMapKeeper'.

    Parameters
    ----------
    operatorsMap_: OperatorsMapping
        Operators mapper.
    """
    keeper.keep = operatorsMap_