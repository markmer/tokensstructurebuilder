import numpy as np


class Logger:

    def __init__(self):
        self.best_fitnesses = []
        self.lengths = []

    def check_individs_length(self, population):
        lengths = np.max(list(map(lambda individ: len(individ.chromo), population.population)))
        self.lengths.append(lengths)