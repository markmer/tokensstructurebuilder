"""
Contains inheritors/implementations of baseline classes for Population.
"""
from buildingBlocks.baseline.Populations import Population
from buildingBlocks.default.Loggers import Logger


class PopulationOfEquations(Population):
    """
    Class with population of Equations.
    """
    def __init__(self, genetic_operators: dict = None, population: list = None,
                 iterations: int = 0):
        super().__init__(genetic_operators=genetic_operators, population=population)
        self.iterations = iterations
        self.loggers = [Logger(), Logger()]

    def _evolutionary_step(self):
        self.apply_operator('regularization')
        self.apply_operator('optimize')
        self.apply_operator('lasso')

        self.loggers[0].check_individs_length(self)

        self.apply_operator('fitness')
        self.apply_operator('restriction')
        self.apply_operator('elitism')
        self.apply_operator('selection')
        self.apply_operator('crossover')
        self.apply_operator('mutation')

        self.loggers[1].check_individs_length(self)

    def evolutionary(self):
        self.apply_operator('initialize')
        for _ in range(self.iterations):
            self._evolutionary_step()
        self.apply_operator('regularization')
        self.apply_operator('lasso')
        self.apply_operator('fitness')
        self.apply_operator('elitism')
