"""
Contains inheritors/implementations of baseline classes for Token.
"""
from copy import deepcopy
from buildingBlocks.baseline.Tokens import TerminalToken, ComplexToken
import numpy as np
from functools import reduce

from numba import njit


class Constants:

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value


constants = Constants()


def set_constants(**kwargs) -> None:
    for key, value in kwargs.items():
        constants[key] = value


class Constant(TerminalToken):
    def __init__(self, number_params=1,
                 params_description=None,
                 params=np.array([1.]), val=None,
                 name_=None, mandatory=0):
        if params_description is None:
            params_description = {
                0: dict(name='Amplitude', bounds=(-1., 1.)),
            }
        super().__init__(number_params=number_params, params_description=params_description,
                         params=params, val=val, name_=name_, mandatory=mandatory)
        self.type = 'Constant'
        # self.init_val = deepcopy(val)
        self.init_val = name_
    # TODO Проверить теорию о влиянии замены значений инит вал на имя в общедоступном словаре констант на скорость \\

    def evaluate(self, params, grid):
        return params[0] * constants[self.init_val]

    def __eq__(self, other):
        if type(self).__name__ == type(other).__name__:
            return self.name_ == other.name_
        return False


class Power(TerminalToken):
    def __init__(self, number_params=2, params_description=None, params=None):
        if params_description is None:
            params_description = {
                0: dict(name='Amplitude', bounds=(-1., 1.)),
                1: dict(name='Power', bounds=(-3., 3.))
            }
        super().__init__(number_params=number_params, params_description=params_description,
                         params=params)
        self.type = "NonPeriodic"

    @staticmethod
    def evaluate(params, t):
        return params[0] * t ** params[1]


class Sin(TerminalToken):
    def __init__(self, number_params=3, params_description=None,
                 params=None, name_='Sin', optimize_id=None):
        if params_description is None:
            params_description = {
                0: dict(name='Amplitude', bounds=(-1., 1.)),
                1: dict(name='Frequency', bounds=(0., float('inf'))),
                2: dict(name='Phase', bounds=(0., 1.))
            }
        super().__init__(number_params=number_params, params_description=params_description,
                         params=params, name_=name_, optimize_id=optimize_id)
        self.type = "Periodic"

    @staticmethod
    def evaluate(params, t):
        # todo maybe problems
        if (params[0:2] == 0).any():
            return np.zeros(t.shape)
        # return params[0] * np.sin(1 * np.pi * (2 * params[1] * t + abs(math.modf(params[2])[0])))
        return params[0] * np.sin(1 * np.pi * (2 * params[1] * t + params[2]))

    def name(self, with_params=False):
        return self.name_ + str(self.params)

    def __eq__(self, other):
        if type(self).__name__ == type(other).__name__:
            self_freq = self.param(name='Frequency')
            other_freq = other.param(name='Frequency')
            self_phase = self.param(name='Phase')
            other_phase = other.param(name='Phase')
            if self_freq == other_freq == 0:
                return True
            if self_phase == other_phase == 0:
                return abs((self_freq - other_freq) / (self_freq + other_freq)) < 0.05
            return (abs((self_freq - other_freq) / (self_freq + other_freq)) < 0.05 and
                    abs((self_phase - other_phase) / (self_phase + other_phase)) < 0.25)
            # return abs((self.param(name='Frequency') - other.param(name='Frequency')) /
            #            (self.param(name='Frequency') + other.param(name='Frequency'))) < 0.05
        return False


class ImpSingle(TerminalToken):
    def __init__(self, number_params=6,
                 params_description=None, params=None):
        if params_description is None:
            params_description = {
                0: dict(name='Amplitude', bounds=(0., 1.)),
                1: dict(name='Pulse start', bounds=(0., float('inf'))),
                2: dict(name='Pulse front duration', bounds=(0., float('inf'))),
                3: dict(name='Pulse recession duration', bounds=(0., float('inf'))),
                4: dict(name='Front power', bounds=(0., 3.)),
                5: dict(name='Recession power', bounds=(0., 3.))
            }
        super().__init__(number_params=number_params, params_description=params_description,
                         params=params)
        self.type = "NonPeriodic"

    @staticmethod
    def evaluate(params, t):
        params[1:4] = np.abs(params[1:4])
        A, T1, T2, T3, p1, p2 = params

        cond1 = (t >= T1) & (t < T1 + T2)
        cond2 = (t >= T1 + T2) & (t <= (T1 + T2 + T3))
        m = np.zeros(len(t))
        if T2 != 0:
            m[cond1] = (np.abs(t[cond1] - T1) / T2) ** np.abs(p1)
        if T3 != 0:
            m[cond2] = (np.abs(t[cond2] - (T1 + T2 + T3)) / T3) ** np.abs(p2)
        return 2 * A * m


class Imp(TerminalToken):
    def __init__(self, number_params=7, params_description=None, params=None, name_='Imp', optimize_id=None):
        if params_description is None:
            params_description = {
                0: dict(name='Amplitude', bounds=(-1., 1.)),
                1: dict(name='Frequency', bounds=(0., float('inf'))),
                2: dict(name='Zero part of period', bounds=(0.05, 0.98)),
                3: dict(name='Front part of pulse duration', bounds=(0.05, 0.98)),
                4: dict(name='Front power', bounds=(0., 3.)),
                5: dict(name='Recession power', bounds=(0., 3.)),
                6: dict(name='Phase', bounds=(0., 1.))
            }
        super().__init__(number_params=number_params, params_description=params_description,
                         params=params, name_=name_, optimize_id=optimize_id)
        self.type = "Periodic"

    @staticmethod
    def evaluate(params, t):
        if (params[0:2] == 0).any():
            return np.zeros(t.shape)
        A = params[0]
        fi = params[-1]

        T = 1 / params[1]
        n1 = np.abs(params[2])
        T1 = n1 * T
        n2 = np.abs(params[3])
        T2 = n2 * (T - T1)
        T3 = T - T1 - T2

        p1 = params[4]
        p2 = params[5]

        t1 = (t + fi * T/2) % T

        cond1 = (t1 >= T1) & (t1 < T1 + T2)
        cond2 = (t1 >= T1 + T2) & (t1 <= T)
        try:
            m = np.zeros(len(t))
            if T2 != 0:
                m[cond1] = (np.abs(t1[cond1] - T1) / T2) ** np.abs(p1)
            if T3 != 0:
                m[cond2] = (np.abs(t1[cond2] - T) / T3) ** np.abs(p2)
        except:
            m = 0
            if cond1:
                m = np.abs((t1 - T1) / T2) ** np.abs(p1)
            elif cond2:
                m = np.abs((t1 - T) / T3) ** np.abs(p2)
        return A * m

    def name(self, with_params=False):
        return self.name_ + str(self.params)


class Product(ComplexToken):

    def __init__(self, number_params=2,
                 params_description=None,
                 params=np.array([1., 3]), val=None,
                 name_=None,
                 subtokens=None):
        if params_description is None:
            params_description = {
                0: dict(name='Amplitude', bounds=(-1., 1.)),
                1: dict(name='max_subtokens_len', bounds=(2, float('inf')))
            }
        super().__init__(number_params=number_params, params_description=params_description,
                         params=params, val=val, name_=name_)
        if subtokens is None:
            subtokens = []
        # self.multipliers = deepcopy(multipliers)
        self.subtokens = subtokens
        self.type = 'Product'

    def add_subtoken(self, token):
        if len(self.subtokens) >= int(self.params[1]):
            return self.set_subtoken(token, idx=np.random.randint(0, len(self.subtokens)))
        token.set_param(1, name='Amplitude')
        self._fix_val = False
        self.subtokens.append(token)
        return

    def evaluate(self, params, grid):
        # self._fix_val = reduce(lambda val, x: val*x,
        #                        list(map(lambda x: x._fix_val, self.subtokens)))
        return params[0] * reduce(lambda val, x: val * x,
                                  list(map(lambda x: x.value(grid), self.subtokens)))

    def name(self, with_params=False):
        # if self.name_ is not None:
        #     return str(self.params[0]) + self.name_
        s = '('
        for i in self.subtokens:
            s += i.name_
        s += ')'
        # if with_params:
        #     return type(self).__name__ + s
        return str(self.params[0]) + type(self).__name__ + s

    def __eq__(self, other):
        if type(self).__name__ == type(other).__name__:
            if len(self.subtokens) == len(other.subtokens):
                for token in self.subtokens:
                    if self.subtokens.count(token) != other.subtokens.count(token):
                        return False
                return True
        return False

# тип соответствия и количество используемых временных параметров
imp_relations = {
    Imp: [ImpSingle, 3],
}


class ImpComplex(ComplexToken):
    def __init__(self, pattern=Imp, imps=None, fix=False):
        self.pattern = pattern.copy()
        self.number_params = self.pattern.number_params
        self.params = self.pattern.params
        self.fix = fix
        self.type = 'ImpComplex'
        if imps is None:
            imps = []
        self.imps = imps
        self.sample_imps = [self.pattern]
        self.value_type = 'norm'

    def name(self, with_params=False):
        if not with_params:
            return 'Complex' + self.pattern.name(with_params)
        s = type(self).__name__ + '(' + 'pattern=' + self.pattern.name(True) + ',imps=['
        for imp in self.imps:
            s += imp.name(True) + ','
        s = s[:-1]
        s += '])'
        return s

    def get_params(self):
        return self.pattern.get_params()

    def put_params(self, params):
        self.pattern.put_params(params)

    def get_imps_bounds(self, t):
        imps_bounds = deepcopy(self.pattern.bounds[:-1])
        imps_bounds[1] = (0, t.max())
        imps_bounds[2] = (0, imps_bounds[1][1] / 4)
        imps_bounds[3] = (0, imps_bounds[1][1] / 4)
        return imps_bounds

    def value(self, grid):
        if self.value_type == 'norm':
            self.init_imps(grid)
            return self.value_imps(grid)
        else:
            return self.sample_value(grid)

    def init_imps(self, t):
        if len(self.imps) != 0:
            return
        # определяем количество параметров отвечающих за время
        nb_of_time_params = imp_relations[type(self.pattern)][1]
        # выносим временные параметры
        T = list(map(lambda x: x, self.pattern.params[1: 1 + nb_of_time_params]))
        # преобразование периметра в частоту
        T[0] = 1 / T[0]
        T1 = deepcopy(T)
        for idx in range(len(T) - 1):
            T[idx] = T1[idx + 1] * (T1[0] - np.sum(T[:idx]))
        T[-1] = T1[0] - np.sum(T[:-1])
        # выделяем полный период add (шаг), начальное значение(от фазы) и порог времени
        add = np.sum(T)
        sm = -self.pattern.params[-1] * add + T[0]
        mxt = t[-1]
        while sm < mxt:
            # расставляем в параметры амплитуду, точки старта sm, и длительности импульсов
            new_params = [self.pattern.params[0], sm]
            for i in range(nb_of_time_params - 1):
                new_params.append(T[1 + i])
            new_params.extend(self.pattern.params[nb_of_time_params + 1: -1])
            # создаем импульс с нужными параметрами
            new_imp = imp_relations[type(self.pattern)][0](new_params)
            # new_imp.bounds = self.get_imps_bounds(t)
            self.imps.append(new_imp)
            sm += add

    def value_imps(self, grid):
        return reduce(lambda val, x: val + x, list(map(lambda x: x.value(grid), self.imps)))

    def init_params(self):
        self.pattern.init_params()

    def make_properties(self):
        mas = []
        for i in range(len(self.imps[0].params)):
            ps = np.array(list(map(lambda x: x.params[i], self.imps)))
            mas.append(ps)
        mas[1] = np.sort(mas[1])
        mas[1] = np.abs(mas[1][1:] - mas[1][:-1])
        return mas

