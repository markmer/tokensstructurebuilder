"""
Contains baseline prototypes of 'Individual' instance in Population.

Classes
----------
Individ
"""
from copy import deepcopy, copy
from buildingBlocks.default.geneticOperators.OperatorsMappings import keeper


class Individ:
    """
    Abstract class.
    Inidivid is a individual in population in the context of evolutionary algorithm.
    This class implements basic necessary functionality like work with chromosomes (whose consist of tokens as gens),
    with fitness of the individual and with influence of genetic operators that changes individual chromosome.
    """
    def __init__(self, genetic_operators: dict = None, chromo: list = None,
                 fitness: float = None, fixes: dict = None):
        """

        Parameters
        ----------
        genetic_operators: dict
            Set of names of genetic operators that can influence the individ.
            These names are mapped to the implementations of the operators in the OperatorsMapping object.
            Form example:
            {
                'operator name for Individ': 'operator name in OperatorsMapping',
                ...
            }
        chromo: list
            List of tokens as gens.
        fitness: float
            Numeric metric of the Individ fitness
        fixes: dict
            Caching influence of genetic operators.
        """
        if genetic_operators is None:
            genetic_operators = {}
        self.genetic_operators = genetic_operators
        if fixes is None:
            fixes = dict(fitness=False)
        self.fixes = fixes
        if chromo is None:
            chromo = []
        self.chromo = chromo
        self.fitness = fitness

    @property
    def genetic_operators(self):
        return self._genetic_operators

    @genetic_operators.setter
    def genetic_operators(self, genetic_operators):
        self._genetic_operators = genetic_operators

    @property
    def chromo(self):
        return self._chromo

    @chromo.setter
    def chromo(self, chromo: list):
        self.change_all_fixes(False)
        self._chromo = chromo

    def set_token(self, token, idx: int):
        self.change_all_fixes(False)
        self.chromo[idx] = token

    def add_tokens(self, tokens):
        self.change_all_fixes(False)
        try:
            self.chromo.extend(tokens)
        except:
            self.chromo.append(tokens)

    @property
    def fitness(self):
        return self._fitness

    @fitness.setter
    def fitness(self, fitness):
        self._fitness = fitness

    def change_all_fixes(self, value: bool = False):
        for key in self.fixes.keys():
            self.fixes[key] = value

    def copy(self):
        new_copy = copy(self)
        new_copy.chromo = deepcopy(new_copy.chromo)
        new_copy.fixes = deepcopy(new_copy.fixes)
        return new_copy

    def apply_operator(self, name: str, *args, **kwargs):
        """
        Apply an operator with the given name.

        Parameters
        ----------
        name: str
            Name of the operator in genetic_operators dict.

        args
        kwargs

        Returns
        -------
        None
        """
        try:
            operator = keeper.keep[self.genetic_operators[name]]
        except KeyError:
            raise KeyError("Operator with name '{}' is not implemented in"
                           " {}.genetic_operators".format(name, type(self).__name__))
        # self.fitness = None
        # for key in self.fixes.keys():
        #     self.fixes[key] = False

        return operator._apply(self, *args, **kwargs)
