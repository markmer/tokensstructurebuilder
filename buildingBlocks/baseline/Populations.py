"""
Contains baseline prototypes of 'Population' instance.

Classes
----------
Population
"""
from buildingBlocks.default.geneticOperators.OperatorsMappings import keeper


class Population:
    """
    Abstract class (methods 'evolutionary' and 'evolutionary_step' must be implemented).
    Population contains set of individuals.
    This class implements basic necessary functionality like work with population and with genetic operators that
    change population (add new individs or change existing ones).
    Methods
    """
    def __init__(self, genetic_operators: dict = None, population: list = None):
        """

        Parameters
        ----------
        genetic_operators: dict
            Set of names of genetic operators that can influence population (individuals in population).
            These names are mapped to the implementations of the operators in the OperatorsMapping object.
            Form example:
            {
                'operator name for Individ': 'operator name in OperatorsMapping',
                ...
            }
        population: list
            List of individuals in population.
        """
        if genetic_operators is None:
            genetic_operators = {}
        self.genetic_operators = genetic_operators
        if population is None:
            population = []
        self.population = population

    @property
    def genetic_operators(self):
        return self._genetic_operators

    @genetic_operators.setter
    def genetic_operators(self, genetic_operators):
        self._genetic_operators = genetic_operators

    def apply_operator(self, name: str, operator_args=None):
        # from additional.default.geneticOperators.OperatorsMappings import keeper
        try:
            operator = keeper.keep[self.genetic_operators[name]]
        except KeyError:
            raise KeyError("Operator with name '{}' is not implemented in"
                           " {}.genetic_operators".format(name, type(self).__name__))
        if operator_args is not None:
            self.population = operator._apply(self.population, operator_args)
        self.population = operator._apply(self.population)

    def evolutionary(self):
        """
        Evolutionary process of the population. Must contain 'evolutionary_step()' method.
        """
        raise NotImplementedError("Define evolution by 'Population.evolutionary_step()'")

    # def evolutionary_step(self):
    #     """
    #     Step of the population evolution.
    #     """
    #     raise NotImplementedError("Determine the development step of one generation "
    #                               "using 'Population.genetic_operators' and 'Population.apply_operator()'")
