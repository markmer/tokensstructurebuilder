"""
Contains baseline prototypes of instance "Genetic Operator" that influence on
individuals and population.
Contains Operators Map that maps names of genetic operators and their implementation.

Classes:
----------
GeneticOperator
    GeneticOperatorIndivid
    GeneticOperatorPopulation
OperatorsMapping
"""
from dataclasses import dataclass
from buildingBlocks.baseline.Wrappers import MapWrapper


class GeneticOperator:
    """
    Abstract class (need to implement method 'apply').
    Operator is applied to some object and change its properties.
    """
    def __init__(self, params: dict = None):
        """

        Parameters
        ----------
        params: dict
            Parameters for operator work.
        """
        if params is None:
            params = {}
        self.params = params

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, params: dict):
        self._params = params

    def _check_params(self, *names):
        params_keys = self.params.keys()
        for name in names:
            assert name in params_keys, "Key {} must be in {}.params".format(name, type(self).__name__)

    def apply(self, target, *args, **kwargs):
        raise NotImplementedError("Genetic Operator must doing something with target")


class GeneticOperatorIndivid(GeneticOperator):
    """
    Genetic Operator influencing object Individ.
    Change Individ, doesn't create a new one.
    """
    def __init__(self, params: dict = None):
        super().__init__(params=params)

    def apply(self, individ, *args, **kwargs) -> None: #TODO (добавить в параметры str name для ind.fixes[name] и
        # вынести в отдельный метод трай модуль для всех операторов. Если имя==Ноне то пропускать метод или
        # реализовать внутри метода)
        raise NotImplementedError("Genetic Operator must doing something with Individ/Population")

    def _apply(self, individ, *args, **kwargs) -> None:
        return self.apply(individ, *args, **kwargs)


class GeneticOperatorPopulation(GeneticOperator):
    """
    Genetic Operator influencing list of Individs in Population.
    May be parallelized.
    May change Individs in population and create new ones. Return new list of Individs.
    """
    def __init__(self, params: dict = None):
        super().__init__(params=params)
        if 'parallelise' not in self.params.keys():
            self.params['parallelise'] = False

    def apply(self, population: list, *args, **kwargs) -> list:
        raise NotImplementedError("Genetic Operator must doing something with Individ/Population")

    def _apply(self, population: list) -> list:
        if self.params['parallelise']:
            for individ in population:
                individ.send_preparing()
            return MapWrapper(type(self).apply, self, population=population)
        return self.apply(population)


@dataclass
class OperatorsMapping:
    """
    Dataclass using as dict.
    Mapping names of operators with their implementations.
    """
    fitnessIndivid: GeneticOperator = GeneticOperator()
    fitnessPopulation: GeneticOperator = GeneticOperator()
    initializeIndivid: GeneticOperator = GeneticOperator()
    initializePopulation: GeneticOperator = GeneticOperator()
    mutationIndivid: GeneticOperator = GeneticOperator()
    mutationPopulation: GeneticOperator = GeneticOperator()
    crossoverIndivid: GeneticOperator = GeneticOperator()
    crossoverPopulation: GeneticOperator = GeneticOperator()
    selectionPopulation: GeneticOperator = GeneticOperator()

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value














