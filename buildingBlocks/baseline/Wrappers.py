"""
Contains tools for parallelization work.
"""
import functools
from multiprocessing import Pool, current_process, active_children
import numpy as np


class WorkersKeeper:
    """
    Class for keeping number of processes-workers.
    """
    workers: int = 0


workersKeeper = WorkersKeeper()
global pool
pool = None


def set_workers(workers: int) -> None:
    """
    Set property workers for object of 'WorkersKeeper' class.

    Parameters
    ----------
    workers: int
        Number of workers.

    """
    workersKeeper.workers = workers
    global pool
    if current_process().name == 'MainProcess':
        if workersKeeper.workers == 0:
            pool = None
        else:
            pool = Pool(workersKeeper.workers)
            print('NEW POOL', flush=True)
            print(pool, flush=True)
            print(current_process(), active_children(), flush=True)


# class MapWrapper:
#
#     def __call__(self, func):
#         if current_process().name != 'MainProcess':
#             @functools.wraps(func)
#             def n_func(self, *args, **kwargs): return []
#             return n_func
#
#         @functools.wraps(func)
#         def wrapper(self, *args, **kwargs):
#             global pool
#             if workersKeeper.workers == 0:
#                 return func(self, kwargs['population'])
#             ret = list(pool.map(functools.partial(func, self),
#                                 np.array_split(kwargs['population'], len(active_children())),
#                                 len(active_children())))
#             ret = [item for sublist in ret for item in sublist]
#             return ret
#         return wrapper

# class MapWrapper:
#
#     def __call__(self, func, *args, **kwargs):
#         if current_process().name != 'MainProcess':
#             return []
#
#         global pool
#         if workersKeeper.workers == 0:
#             return func(self, kwargs['population'])
#         ret = list(pool.map(functools.partial(func, self),
#                             np.array_split(kwargs['population'], len(active_children())),
#                             len(active_children())))
#         ret = [item for sublist in ret for item in sublist]
#         return ret


def MapWrapper(func, self, *args, **kwargs):
    """
    Decorator for parallelizing method 'apply' of object of class 'GeneticOperatorPopulation'.

    Parameters
    ----------
    func: method
        Method to wrap.
    self:
        Object with given method.
    kwargs:
        population: list
            List of individuals.
    Returns
    -------
    Wrapped method.
    """
    if current_process().name != 'MainProcess':
        return []

    global pool
    if workersKeeper.workers == 0:
        return func(self, kwargs['population'])
    ret = list(pool.map(functools.partial(func, self),
                        np.array_split(kwargs['population'], len(active_children())),
                        len(active_children())))
    ret = [item for sublist in ret for item in sublist]
    return ret
